import { stringify } from 'qs';
import request from '@/utils/request';
import { unstable_batchedUpdates } from 'react-dom';

// List the index for the record type
export async function recordIndex(params, type) {
  let filter = ''
  if (params.filter) {
    filter = '&filter='+params.filter;
  }
  let include = ''
  if (params.include) {
    include = '&include='+params.include;
  }
  let sort = (params.sort ? params.sort : 'created')
  let direction = (params.direction ? params.direction : 'asc')
  let page = (params.page ? params.page : '1')
  let limit = (params.limit ? params.limit : '100')
  return request('/server/'+type+'?api_key=test&sort='+sort+'&direction='+direction+'&limit='+limit+'&page='+page+filter+include);
}

// Get a record
export async function recordView(params, type) {
  let filter = ''
  if (params.filter) {
    filter = '&filter='+params.filter;
  }
  let include = ''
  if (params.include) {
    include = '&include='+params.include;
  }
  return request('/server/'+type+'/view/'+params.id+'?api_key=test'+include);
}

export async function tableEnums(type) {
  return request('/server/'+type+'/enums?api_key=test');
}

// Add a new record
export async function addRecord(params, type) {
  return request('/server/'+type+'/add?api_key=test', {
    method: 'POST',
    headers: {
      "Accept": "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json"
    },
    body: {
      data: {
        type: type,
        attributes: {
          ...params,
        }
      },
    },
  });
}

// Update a record
export async function updateRecord(params, type) {
  var id = params.id;
  delete params.id;

  return request('/server/'+type+'/edit/'+id+'?api_key=test', {
    method: 'POST',
    headers: {
      "Accept": "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json"
    },
    body: {
      data: {
        id: id,
        type: type,
        attributes: {
          ...params,
        }
      },
    },
  });
}

export async function removeRecord(params,type) {
  var id = params.id;

  return request('/server/'+type+'/delete/'+id+'?api_key=test', {
    method: 'POST',
    headers: {
      "Accept": "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json"
    },
    body: {
      data: {
        id: id,
        type: type,
      },
      method: 'delete',
    },
  });
}

export async function registerUser(params) {
  return request('/server/users/users/register', {
    method: 'POST',
    body: params,
    headers: {
      "Accept": "application/json"
    }
  });
}

export async function queryProjectNotice() {
  return request('/api/project/notice');
}

export async function queryActivities() {
  return request('/api/activities');
}

export async function queryRule(params) {
  return request(`/api/rule?${stringify(params)}`);
}

export async function removeRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'delete',
    },
  });
}

export async function addRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'post',
    },
  });
}

export async function updateRule(params) {
  return request('/api/rule', {
    method: 'POST',
    body: {
      ...params,
      method: 'update',
    },
  });
}

export async function fakeSubmitForm(params) {
  return request('/api/forms', {
    method: 'POST',
    body: params,
  });
}

export async function fakeChartData() {
  return request('/api/fake_chart_data');
}

export async function queryTags() {
  return request('/api/tags');
}

export async function queryBasicProfile() {
  return request('/api/profile/basic');
}

export async function queryAdvancedProfile() {
  return request('/api/profile/advanced');
}

export async function queryFakeList(params) {
  return request(`/api/fake_list?${stringify(params)}`);
}

export async function removeFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'delete',
    },
  });
}

export async function addFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'post',
    },
  });
}

export async function updateFakeList(params) {
  const { count = 5, ...restParams } = params;
  return request(`/api/fake_list?count=${count}`, {
    method: 'POST',
    body: {
      ...restParams,
      method: 'update',
    },
  });
}

export async function fakeAccountLogin(params) {
  // return request('/api/login/account', {
  //   method: 'POST',
  //   body: params,
  // });
  return request('/server/users/users/login', {
    headers: {
      "Accept": "application/vnd.api+json",
      "Content-Type": "application/vnd.api+json"
    },
    method: 'POST',
    body: params,
  });
}

export async function fakeRegister(params) {
  return request('/api/register', {
    method: 'POST',
    body: params,
  });
}

export async function queryNotices() {
  return request('/api/notices');
}

export async function getFakeCaptcha(mobile) {
  return request(`/api/captcha?mobile=${mobile}`);
}
