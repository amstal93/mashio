import { recordIndex, tableEnums, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'taps',

  state: {
    tap: [],
  },

  effects: {
    *index({ payload }, { call, put }) {
      // Get the index listing
      const response = yield call(recordIndex, payload, 'taps');
      yield put({
        type: 'queryList',
        payload: response
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'taps');
      yield put({
        type: 'appendList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      let record = payload;
      
      const response = yield call(callback, payload, 'taps'); // post

      // Get record index to overwrite everything.
      // Not the most efficient, could just update the needed record. Works for now though.
      const records = yield call(recordIndex, payload, 'taps');

      yield put({
        type: 'queryList',
        payload: records,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload.data)) {
        console.log(action.payload.data)
        if (action.payload.included) {
          action.payload.data.forEach((item, index) => {
            if (item.relationships) {
              var batch = action.payload.included.find(obj => {
                return obj.type === "batches" && obj.id === item.relationships.batch.data.id;
              })
              item.batch = batch;
            }
          });
        }
        return {
          ...state,
          tap: action.payload.data,
        };
      } else {
        return state
      }
    },
    appendList(state, action) {
      return {
        ...state,
        tap: state.tap.concat(action.payload),
      };
    },
  },
};
