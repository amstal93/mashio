import { recordIndex, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'style',

  state: {
    style: [],
  },

  effects: {
    *fetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'styles');
      response.data.meta = response.meta;
      yield put({
        type: 'queryList',
        payload: response
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'styles');
      yield put({
        type: 'appendList',
        payload: response
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      
      const response = yield call(callback, payload, 'styles'); // post

      // Get record index to overwrite everything.
      // Not the most efficient, could just update the needed record. Works for now though.
      const records = yield call(recordIndex, payload, 'styles');

      yield put({
        type: 'queryList',
        payload: records,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload.data)) {
        action.payload.data.meta = action.payload.meta
        
        action.payload.data.forEach((item, index) => {
          var category = action.payload.included.find(obj => {
            return obj.type === "categories" && obj.id === item.relationships.category.data.id;
          })
          item.category = category;
        });

        return {
          ...state,
          style: action.payload.data,
        };
      } else {
        return state
      }
    },
    appendList(state, action) {
      return {
        ...state,
        style: state.style.concat(action.payload),
      };
    },
  },
};
