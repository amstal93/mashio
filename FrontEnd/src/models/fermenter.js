import { recordIndex, tableEnums, removeRecord, addRecord, updateRecord } from '@/services/api';

export default {
  namespace: 'fermenter',

  state: {
    fermenter: [],
  },

  effects: {
    *index({ payload }, { call, put }) {
      // Get the index listing
      const response = yield call(recordIndex, payload, 'fermenters');
      response.data.meta = response.meta;
      response.data.included = response.included;

      const enums = yield call(tableEnums, 'fermenters');
      response.data.enums = enums;
      
      yield put({
        type: 'queryList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *appendFetch({ payload }, { call, put }) {
      const response = yield call(recordIndex, payload, 'fermenters');
      yield put({
        type: 'appendList',
        payload: Array.isArray(response.data) ? response.data : [],
      });
    },
    *submit({ payload }, { call, put }) {
      let callback;
      if (payload.id) {
        callback = (payload.delete == true) ? removeRecord : updateRecord;
      } else {
        callback = addRecord;
      }
      let record = payload;
      
      const response = yield call(callback, payload, 'fermenters'); // post

      // Get record index to overwrite everything.
      // Not the most efficient, could just update the needed fields. Works for now though.
      const records = yield call(recordIndex, payload, 'fermenters');
      const enums = yield call(tableEnums, 'fermenters');
      records.data.enums = enums;

      yield put({
        type: 'queryList',
        payload: records,
      });
    },
  },

  reducers: {
    queryList(state, action) {
      if (Array.isArray(action.payload)) {
        action.payload.forEach((item, index) => {
          if (item.relationships) {
            var batch = action.payload.included.find(obj => {
              return obj.type === "batches" && obj.id === item.relationships.batch.data.id;
            })
            item.batch = batch;
          }
        });

        action.payload.forEach((item, index) => {
          item.attributes.statusesText = action.payload.enums.statuses[item.attributes.status]
          item.attributes.typesText = action.payload.enums.types[item.attributes["fermenter-type"]]
        });

        return {
          ...state,
          fermenter: action.payload,
        };
      } else if (Array.isArray(action.payload.data)) {
        action.payload.data.meta = action.payload.meta
        action.payload.data.forEach((item, index) => {
          if (item.relationships) {
            var batch = action.payload.included.find(obj => {
              return obj.type === "batches" && obj.id === item.relationships.batch.data.id;
            })
            item.batch = batch;
          }
        });

        action.payload.forEach((item, index) => {
          item.attributes.statusesText = action.payload.enums.statuses[item.attributes.status]
          item.attributes.typesText = action.payload.enums.types[item.attributes["fermenter-type"]]
        });

        action.payload.data.included = action.payload.included
        return {
          ...state,
          fermenter: action.payload.data,
        };
      } else {
        return state
      }
    },
    appendList(state, action) {
      return {
        ...state,
        fermenter: state.fermenter.concat(action.payload),
      };
    },
  },
};
