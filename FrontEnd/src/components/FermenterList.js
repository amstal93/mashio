import { Table, Popconfirm, Button } from 'antd';

const FermenterList = ({ onDelete, fermenters }) => {
  const columns = [{
    title: 'Name',
    dataIndex: 'name',
  }, {
    title: 'Actions',
    render: (text, record) => {
      return (
        <Popconfirm title="Delete?" onConfirm={() => onDelete(record.id)}>
    <Button>Delete</Button>
      </Popconfirm>
    );
    },
  }];
  return (
    <Table
      dataSource={fermenters}
      columns={columns}
    />
  );
};

export default FermenterList;