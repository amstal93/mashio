module.exports = {
  "navTheme": "dark",
  "primaryColor": "#2F54EB",
  "layout": "topmenu",
  "contentWidth": "Fixed",
  "fixedHeader": false,
  "autoHideHeader": false,
  "fixSiderbar": false,
  "enums": {
    "status": {
      "0": "Dirty"
    }
  },
  "collapse": true
}