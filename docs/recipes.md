# Recipes

Recipes include the entire brew process, from starting a yeast starter to packaging.

Recipe steps are either pre- or post-brew day steps, or brew-day steps.

Pre- and post-brew day steps and are offset by days +/- from the brew day, and will appear on your calendar and as tasks to mark as completed.

Brew-day steps are offset from the previous step in minutes. So a 60 minute hop addition on a 90 minute boil would be 30 minutes after the `Boil Start` step is completed.