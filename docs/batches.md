# Batches

The batches function can be used to track each brew day, fermentation, and even consumption stats for every batch of beer you make.