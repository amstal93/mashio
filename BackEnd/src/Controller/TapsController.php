<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Taps Controller
 *
 * @property \App\Model\Table\TapsTable $Taps
 *
 * @method \App\Model\Entity\Tap[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TapsController extends AppController
{
    // @codeCoverageIgnoreStart
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
              'actions' => [
              'Crud.Index',
              'Crud.View',
              'Crud.Edit',
              'Crud.Add',
              'Crud.Delete'
              ],
              'listeners' => [
              'CrudJsonApi.JsonApi',
              'CrudJsonApi.Pagination',
              'Crud.ApiPagination',
              'Crud.Search'
              ]
            ]
        );
        $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }

    public function index()
    {
      $this->Crud->on('beforePaginate', function (\Cake\Event\Event $event) {
        $event->getSubject()->query->contain([
          'Batches',
          'Batches.Recipes',
        ]);
      });
      return $this->Crud->execute();
    }
    // @codeCoverageIgnoreEnd
}
