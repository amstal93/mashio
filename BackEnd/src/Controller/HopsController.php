<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\OwnRecordsTrait;
use App\Controller\Traits\EnumsTrait;

/**
 * Hops Controller
 *
 * @property \App\Model\Table\HopsTable $Hops
 *
 * @method \App\Model\Entity\Hop[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class HopsController extends AppController
{
    // @codeCoverageIgnoreStart
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Edit',
                'Crud.Add',
                'Crud.Delete'
                ],
                'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
                ]
            ]
        );
        $this->Crud->setConfig(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
