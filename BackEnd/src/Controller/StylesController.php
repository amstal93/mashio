<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Styles Controller
 *
 * @property \App\Model\Table\StylesTable $Styles
 *
 * @method \App\Model\Entity\Style[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StylesController extends AppController
{
    // @codeCoverageIgnoreStart
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
              'actions' => [
                'Crud.Index',
                'Crud.View',
              ],
              'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
              ]
            ]
        );
        $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
