<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\EnumsTrait;

/**
 * Yeasts Controller
 *
 * @property \App\Model\Table\YeastsTable $Yeasts
 *
 * @method \App\Model\Entity\Yeast[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class YeastsController extends AppController
{

    // @codeCoverageIgnoreStart
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                    'Crud.Index',
                    'Crud.View',
                    'Crud.Edit',
                    'Crud.Add',
                    'Crud.Delete'
                ],
                'listeners' => [
                    'CrudJsonApi.JsonApi',
                    'CrudJsonApi.Pagination',
                    'Crud.ApiPagination',
                    'Crud.Search'
                ]
            ]
        );
    }
    // @codeCoverageIgnoreEnd
}
