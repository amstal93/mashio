<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Controller\Traits\OwnRecordsTrait;
use App\Controller\Traits\EnumsTrait;

/**
 * Batches Controller
 *
 * @property \App\Model\Table\BatchesTable $Batches
 *
 * @method \App\Model\Entity\Batch[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BatchesController extends AppController
{
    // @codeCoverageIgnoreStart
    use OwnRecordsTrait;
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Edit',
                'Crud.Add',
                'Crud.Delete'
                ],
                'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
                ]
            ]
        );
        // $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
        $this->Crud->setConfig('listeners.jsonApi.include', [
            'recipe',
            'fermenter',
            'tap',
          ]);
    }

    public function index()
    {
      $this->Crud->on('beforePaginate', function (\Cake\Event\Event $event) {
        $event->getSubject()->query->contain([
          'Recipes',
          'Fermenters',
          'Taps',
        ]);
      });
      return $this->Crud->execute();
    }

    public function view()
    {
      $this->Crud->on('beforeFind', function (\Cake\Event\Event $event) {
        $event->getSubject()->query->contain([
          'Recipes'
        ]);
      });
      return $this->Crud->execute();
    }
    // @codeCoverageIgnoreEnd
}
