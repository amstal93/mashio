<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * RecipeEntries Controller
 *
 * @property \App\Model\Table\RecipeEntriesTable $RecipeEntries
 *
 * @method \App\Model\Entity\RecipeEntry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecipeEntriesController extends AppController
{
    // @codeCoverageIgnoreStart
    use OwnRecordsTrait;
    use EnumsTrait;

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent(
            'Crud.Crud',
            [
                'actions' => [
                'Crud.Index',
                'Crud.View',
                'Crud.Edit',
                'Crud.Add',
                'Crud.Delete'
                ],
                'listeners' => [
                'CrudJsonApi.JsonApi',
                'CrudJsonApi.Pagination',
                'Crud.ApiPagination',
                'Crud.Search'
                ]
            ]
        );
        $this->Crud->config(['listeners.jsonApi.exceptionRenderer' => 'App\Error\JsonApiExceptionRenderer']);
    }
    // @codeCoverageIgnoreEnd
}
