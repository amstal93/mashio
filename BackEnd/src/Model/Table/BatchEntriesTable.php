<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
use Cake\Event\EventManager;

/**
 * BatchEntries Model
 *
 * @property \App\Model\Table\RecipeEntriesTable|\Cake\ORM\Association\BelongsTo $RecipeEntries
 * @property \App\Model\Table\BatchesTable|\Cake\ORM\Association\BelongsTo $Batches
 *
 * @method \App\Model\Entity\BatchEntry get($primaryKey, $options = [])
 * @method \App\Model\Entity\BatchEntry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BatchEntry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BatchEntry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BatchEntry|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BatchEntry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BatchEntry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BatchEntry findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BatchEntriesTable extends Table
{
    public $enums = array(
        'status' => array(
            'Planned',
            'Completed',
            'Canceled'
        )
    );

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('batch_entries');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Search.Search');

        $this->belongsTo(
            'RecipeEntries', [
            'foreignKey' => 'recipe_entry_id'
            ]
        );
        $this->belongsTo(
            'Batches', [
            'foreignKey' => 'batch_id'
            ]
        );

        $this->addBehavior('Enum');

        $this->addBehavior('Muffin/Trash.Trash');
        
    }

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        $validator
            ->integer('status')
            ->allowEmpty('status');

        $validator
            ->date('date')
            ->allowEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_entry_id'], 'RecipeEntries'));
        $rules->add($rules->existsIn(['batch_id'], 'Batches'));
        return $rules;
    }

    public function afterSave($event, $batchEntry, $options)
    {
        if ($batchEntry->status == $this->enumValueToKey('status', 'Completed')) {
            $event = new Event(
                'Model.BatchEntry.completed', $this, 
                [
                    'batchEntry' => $batchEntry
                ]
            );
            $this->getEventManager()->dispatch($event);
        }
    }
}
