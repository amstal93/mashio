<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Hops Model
 *
 * @method \App\Model\Entity\Hop get($primaryKey, $options = [])
 * @method \App\Model\Entity\Hop newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Hop[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Hop|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Hop|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Hop patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Hop[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Hop findOrCreate($search, callable $callback = null, $options = [])
 */
class HopsTable extends Table
{
    // @codeCoverageIgnoreStart
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('hops');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Search.Search');
    }

    public function searchManager()
    {
        $searchManager = $this->behaviors()->Search->searchManager();
        $searchManager->like(
            'filter', [
            'before' => true,
            'after' => true,
            'field' => [$this->aliasField('name')]
            ]
        );

        return $searchManager;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 255)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->integer('origin')
            ->requirePresence('origin', 'create')
            ->notEmpty('origin');

        $validator
            ->scalar('usage')
            ->maxLength('usage', 255)
            ->requirePresence('usage', 'create')
            ->notEmpty('usage');

        $validator
            ->decimal('alpha_acids')
            ->requirePresence('alpha_acids', 'create')
            ->notEmpty('alpha_acids');

        $validator
            ->decimal('beta_acids')
            ->requirePresence('beta_acids', 'create')
            ->notEmpty('beta_acids');

        $validator
            ->scalar('notes')
            ->maxLength('notes', 16777215)
            ->allowEmpty('notes');

        $validator
            ->scalar('substitutes')
            ->requirePresence('substitutes', 'create')
            ->notEmpty('substitutes');

        $validator
            ->scalar('style_use')
            ->requirePresence('style_use', 'create')
            ->notEmpty('style_use');

        return $validator;
    }
}
