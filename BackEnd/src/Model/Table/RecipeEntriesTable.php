<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RecipeEntries Model
 *
 * @property \App\Model\Table\RecipesTable|\Cake\ORM\Association\BelongsTo $Recipes
 *
 * @method \App\Model\Entity\RecipeEntry get($primaryKey, $options = [])
 * @method \App\Model\Entity\RecipeEntry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RecipeEntry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RecipeEntry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeEntry|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RecipeEntry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeEntry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RecipeEntry findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipeEntriesTable extends Table
{
    public $enums = array(
        'type' => array(
            'Make Yeast Starter',
            'Cold Crash Yeast Starter',
            'Collect Water',
            'Heat Strike Water',
            'Mash In',
            'Mash Out',
            'Boil Start',
            'Add Hops',
            'Boil End',
            'Whirlpool',
            'Sparge',
            'Vorlauf',
            'Chill Wort',
            'Pitch Yeast',
            'Dry Hop',
            'Rack To Secondary Fermenter',
            'Cold Crash',
            'Package'
        )
    );

    /**
     * Initialize method
     *
     * @param  array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipe_entries');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo(
            'Recipes', [
            'foreignKey' => 'recipe_id'
            ]
        );

        $this->addBehavior('Enum');

        $this->addBehavior('Muffin/Trash.Trash');
    }

    /**
     * Default validation rules.
     *
     * @param  \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->dateTime('deleted')
            ->allowEmpty('deleted');

        $validator
            ->integer('type')
            ->allowEmpty('type');

        $validator
            ->integer('minutes')
            ->allowEmpty('minutes');

        $validator
            ->integer('days')
            ->allowEmpty('days');

        $validator
            ->scalar('title')
            ->maxLength('title', 255)
            ->allowEmpty('title');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param  \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipe_id'], 'Recipes'));

        return $rules;
    }
}
