<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Hop Entity
 *
 * @property int $id
 * @property string $name
 * @property int $origin
 * @property string $usage
 * @property float $alpha_acids
 * @property float $beta_acids
 * @property string|null $notes
 * @property string $substitutes
 * @property string $style_use
 */
class Hop extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'origin' => true,
        'usage' => true,
        'alpha_acids' => true,
        'beta_acids' => true,
        'notes' => true,
        'substitutes' => true,
        'style_use' => true
    ];
}
