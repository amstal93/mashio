<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * RecipeEntry Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property int|null $type
 * @property int|null $recipe_id
 * @property int|null $minutes
 * @property int|null $days
 * @property string|null $title
 *
 * @property \App\Model\Entity\Recipe $recipe
 */
class RecipeEntry extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'type' => true,
        'recipe_id' => true,
        'minutes' => true,
        'days' => true,
        'title' => true,
        'recipe' => true
    ];
}
