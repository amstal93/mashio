<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;

/**
 * Fermenter Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property \Cake\I18n\FrozenTime|null $deleted
 * @property string|null $name
 * @property string|null $description
 * @property float|null $capacity
 * @property int|null $batch_id
 *
 * @property \App\Model\Entity\Batch $batch
 */
class Fermenter extends Entity
{

    protected $_virtual = ['fill_percent'];

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*'=>true,'id'=>false
    ];

    public function _getFillPercent()
    {
        if (isset($this->_properties['batch']) && isset($this->_properties['capacity'])) {
            return round($this->_properties['batch']->volume / $this->_properties['capacity'] * 100);
        } else {
            return 0;
        }
    }

    // @codeCoverageIgnoreStart
}
