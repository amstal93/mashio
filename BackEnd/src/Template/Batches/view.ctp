<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Batch $batch
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Batch'), ['action' => 'edit', $batch->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Batch'), ['action' => 'delete', $batch->id], ['confirm' => __('Are you sure you want to delete # {0}?', $batch->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Batches'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Batch'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="batches view large-9 medium-8 columns content">
    <h3><?= h($batch->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Recipe') ?></th>
            <td><?= $batch->has('recipe') ? $this->Html->link($batch->recipe->name, ['controller' => 'Recipes', 'action' => 'view', $batch->recipe->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td>
                <?= $this->Enum->enumKeyToValue('Batches','status', $batch->status) ?>
                    <?= $this->Form->create($batch,['url'=>'/batch-entries/edit/'.$nextBatchEntry->id,'type'=>'post']) ?>
                    <?php if ($batch->status == $this->Enum->enumValueToKey('Batches','status', 'Packaged')) { ?>
                        <?= $this->Form->control('tap_id', ['options'=>$taps,'empty'=>'Select a Tap']);?>
                    <?php } ?>
                    <?= $this->Form->control('date',[
                        'class'=>'form-control form-control-inline input-medium date-picker',
                        'type'=>'text'
                    ]);?>
                    <?=$this->Form->hidden('status',['value'=>$this->Enum->enumValueToKey('BatchEntries','status','Completed')])?>
                    <span class="help-block">Select date this step was completed</span>
                    <?= $this->Form->submit('Mark `'.$this->Enum->enumKeyToValue('RecipeEntries','type',$nextBatchEntry->recipe_entry->type).'` complete',['class'=>'btn btn-success btn-xs fa fa-wine-glass']); echo $this->Form->end();?>

            </td>
        </tr>
        <?php if ($batch->status == $this->Enum->enumValueToKey('Batches','status', 'Kicked')) { ?>
            <tr>
                <th scope="row"><?= __('Pints per Day') ?></th>
                <td><?= $this->Number->format($batch->pints/$batch->days_tapped) ?></td>
            </tr>
        <?php } ?>
        <tr>
            <th scope="row"><?= __('Brew Date') ?></th>
            <td><?= $batch->brew_date ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Volume') ?></th>
            <td>Planned: <?= $this->Number->format($batch->recipe->volume)?> Actual: <?= $this->Number->format($batch->volume) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('OG') ?></th>
            <td>Planned: <?= $this->Number->format($batch->recipe->og) ?> Actual: <?= $this->Number->format($batch->og) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('FG') ?></th>
            <td>Planned: <?= $this->Number->format($batch->recipe->fg) ?> Actual: <?= $this->Number->format($batch->og) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('ABV') ?></th>
            <td>
                Planned: <?php if ($batch->recipe->og && $batch->recipe->fg) { echo $this->Calculation->getABV($batch->recipe->og, $batch->recipe->fg)."%"; } else { echo "?"; } ?>
                Actual: <?php if ($batch->og && $batch->fg) { echo $this->Calculation->getABV($batch->og, $batch->fg)."%"; } else { echo "?"; } ?>
            </td>
        </tr>
    </table>
    <div class="row mt">
        <div class="col-md-12">
            <div class="content-panel">
                <h4><i class="fa fa-angle-right"></i> Brew Log</h4><hr><table class="table table-striped table-advance table-hover">
                    <thead>
                    <tr>
                        <th><i class="fa fa-check"></i> Task</th>
                        <th><i class="fa fa-calendar"></i> Date</th>
                        <th><i class=" fa fa-edit"></i> Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($batch->batch_entries as $entry) { ?>
                        <tr>
                            <td><?=$this->Enum->enumKeyToValue('RecipeEntries','type',$entry->recipe_entry->type)?></td>
                            <td><?=($entry->status==$this->Enum->enumKeyToValue('BatchEntries','status','Completed'))?'Scheduled for ':'Completed '?><?=$this->Time->timeAgoInWords($entry->display_date)?></td>
                            <td><?php switch($entry->status) { 
                                case $this->Enum->enumValueToKey('BatchEntries','status','Planned'):?>
                                <span class="label label-info label-mini">Planned</span>
                            <?php break;
                                case $this->Enum->enumValueToKey('BatchEntries','status','Completed'):?>
                                <span class="label label-success label-mini">Completed</span>
                            <?php break;
                            } ?>
                            </td>
                        </tr>
                    <?php } ?> 
                    </tbody>
                </table>
            </div><!-- /content-panel -->
        </div><!-- /col-md-12 -->
    </div>
</div>
