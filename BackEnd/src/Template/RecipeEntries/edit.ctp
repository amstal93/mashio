<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RecipeEntry $recipeEntry
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $recipeEntry->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $recipeEntry->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Recipe Entries'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="recipeEntries form large-9 medium-8 columns content">
    <?= $this->Form->create($recipeEntry) ?>
    <fieldset>
        <legend><?= __('Edit Recipe Entry') ?></legend>
        <?php
            echo $this->Form->select('type', $this->Enum->selectValues('RecipeEntries', 'type'));
            echo $this->Form->control('minutes');
            echo $this->Form->control('days');
            echo $this->Form->control('title');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
