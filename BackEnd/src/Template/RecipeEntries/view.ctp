<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RecipeEntry $recipeEntry
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Recipe Entry'), ['action' => 'edit', $recipeEntry->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Recipe Entry'), ['action' => 'delete', $recipeEntry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $recipeEntry->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Recipe Entries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe Entry'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Recipes'), ['controller' => 'Recipes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Recipe'), ['controller' => 'Recipes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="recipeEntries view large-9 medium-8 columns content">
    <h3><?= h($recipeEntry->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Recipe') ?></th>
            <td><?= $recipeEntry->has('recipe') ? $this->Html->link($recipeEntry->recipe->name, ['controller' => 'Recipes', 'action' => 'view', $recipeEntry->recipe->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($recipeEntry->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($recipeEntry->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Type') ?></th>
            <td><?= $this->Number->format($recipeEntry->type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Minutes') ?></th>
            <td><?= $this->Number->format($recipeEntry->minutes) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Days') ?></th>
            <td><?= $this->Number->format($recipeEntry->days) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($recipeEntry->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($recipeEntry->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Deleted') ?></th>
            <td><?= h($recipeEntry->deleted) ?></td>
        </tr>
    </table>
</div>
