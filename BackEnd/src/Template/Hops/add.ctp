<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Hop $hop
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Hops'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="hops form large-9 medium-8 columns content">
    <?= $this->Form->create($hop) ?>
    <fieldset>
        <legend><?= __('Add Hop') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('origin');
            echo $this->Form->control('usage');
            echo $this->Form->control('alpha_acids');
            echo $this->Form->control('beta_acids');
            echo $this->Form->control('notes');
            echo $this->Form->control('substitutes');
            echo $this->Form->control('style_use');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
