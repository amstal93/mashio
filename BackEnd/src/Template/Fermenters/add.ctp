<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Fermenter $fermenter
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Fermenters'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Batches'), ['controller' => 'Batches', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Batch'), ['controller' => 'Batches', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fermenters form large-9 medium-8 columns content">
    <?= $this->Form->create($fermenter) ?>
    <fieldset>
        <legend><?= __('Add Fermenter') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('capacity');
            echo $this->Form->select('status', $this->Enum->selectValues('Fermenters', 'status'));
            echo $this->Form->select('type', $this->Enum->selectValues('Fermenters', 'type'));
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
