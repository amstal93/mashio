<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BatchesController;
use Cake\TestSuite\IntegrationTestCase;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Controller\BatchesController Test Case
 */
class EnumsTraitTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Batches',
        'app.Users',
        'app.recipe_entries',
        'app.recipes',
        'app.batch_entries',
        'app.fermenters',
        'app.measurements',
    ];

    public function setUp() {
        parent::setUp();
        $this->Users = TableRegistry::get('Users');
        $user = $this->Users->newEntity([
            'api_token' => 'test',
            'active' => 1,
            'role' => 'admin',
            'username' => 'test',
            'password' => 'test',
        ]);
        $this->Users->save($user);

        $this->Batches = TableRegistry::get('Batches');
        $this->batch = $this->Batches->newEntity([
            'user_id' => $user->id
        ]);
        $this->Batches->save($this->batch);

        $this->configRequest([
            'headers' => [
                'Accept' => 'application/vnd.api+json'
            ]
        ]);
        $this->disableErrorHandlerMiddleware();

        $this->session([
            'Auth' => [
                'User' => [
                    'id' => $user->id,
                    'username' => 'testing',
                    // other keys.
                ]
            ]
        ]);
    }

    public function testIndex()
    {
        $result = $this->get('/batches/enums?api_key=test');
        $this->assertResponseOk();
    }
}
