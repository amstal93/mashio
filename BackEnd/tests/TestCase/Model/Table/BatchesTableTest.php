<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Entity\Batch;
use App\Model\Table\BatchesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BatchesTable Test Case
 */
class BatchesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BatchesTable
     */
    public $Batches;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.batches',
        'app.recipes',
        'app.recipe_entries',
        'app.batch_entries',
        'app.fermenters',
        'app.measurements',
        'app.taps'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Batches') ? [] : ['className' => BatchesTable::class];
        $this->Batches = TableRegistry::getTableLocator()->get('Batches', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Batches);

        parent::tearDown();
    }

    /**
     * Test beforeSave method
     *
     * @return void
     */
    public function testBeforeSave()
    {
        $batch = new Batch();
        $this->Batches->save($batch);
        $this->assertEquals($batch->tap_id, null);
        $this->assertEquals($batch->status, $this->Batches->enumValueToKey('status','Planned'));

        $batch->tap_id = 1;
        $this->Batches->save($batch);
        $this->assertEquals($batch->status, $this->Batches->enumValueToKey('status','Tapped'));

        $batch->status = $this->Batches->enumValueToKey('status','Kicked');
        $this->Batches->save($batch);
        $this->assertEquals($batch->tap_id, null);
    }
}
