<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Style;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Style Test Case
 */
class StyleTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Style
     */
    public $Style;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Style = new Style();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Style);

        parent::tearDown();
    }

    /**
     * Test _getColor method
     *
     * @return void
     */
    public function testGetColor()
    {
        $this->assertEquals($this->Style->color,false);

        $this->Style->srm_max = 2;
        $this->assertEquals($this->Style->color,'#ffff45');
    }
}
