<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Fermenter;
use Cake\TestSuite\TestCase;
use Cake\ORM\TableRegistry;

/**
 * App\Model\Entity\Fermenter Test Case
 */
class FermenterTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Entity\Fermenter
     */
    public $Fermenter;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.batches',
        'app.fermenters',
        'app.recipes',
        'app.recipe_entries',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Fermenter = new Fermenter();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Fermenter);

        parent::tearDown();
    }

    /**
     * Test _getFillPercent method
     *
     * @return void
     */
    public function testGetFillPercent()
    {
        $this->Fermenters = TableRegistry::get('Fermenters');
        $fermenter = $this->Fermenters->newEntity([
            'capacity' => 5
        ]);
        $this->Fermenters->save($fermenter);

        $this->Batches = TableRegistry::get('Batches');
        $batch = $this->Batches->newEntity([
            'volume' => 4,
            'fermenter_id' => $fermenter->id
        ]);
        $this->Batches->save($batch);

        $fermenter = $this->Fermenters->get($fermenter->id,[
            'contain' => ['Batches']
        ]);
        $this->assertEquals(80,$fermenter->fillPercent);

        $fermenter->capacity = null;
        $this->Fermenters->save($fermenter);

        $this->assertEquals(0,$fermenter->fillPercent);
    }
}
