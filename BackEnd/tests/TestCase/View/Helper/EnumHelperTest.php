<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\EnumHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\EnumHelper Test Case
 */
class EnumHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\EnumHelper
     */
    public $Enum;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Enum = new EnumHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Enum);

        parent::tearDown();
    }

    /**
     * Test enumValueToKey method
     *
     * @return void
     */
    public function testEnumValueToKey()
    {
        $this->assertEquals(0,$this->Enum->enumValueToKey('fermenters','status','Empty (Dirty)'));
    }

    /**
     * Test enumKeyToValue method
     *
     * @return void
     */
    public function testEnumKeyToValue()
    {
        $this->assertEquals('Empty (Dirty)',$this->Enum->enumKeyToValue('fermenters','status',0));
    }

    /**
     * Test selectValues method
     *
     * @return void
     */
    public function testSelectValues()
    {
        $this->assertEquals(9,count($this->Enum->selectValues('fermenters','status')));
    }
}
