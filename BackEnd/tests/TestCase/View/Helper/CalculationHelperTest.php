<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\CalculationHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

/**
 * App\View\Helper\CalculationHelper Test Case
 */
class CalculationHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\CalculationHelper
     */
    public $Calculation;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->Calculation = new CalculationHelper($view);
        $this->Batches = TableRegistry::get('Batches');
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Calculation);

        parent::tearDown();
    }

    /**
     * Test getABV method
     *
     * @return void
     */
    public function testGetABV()
    {
        $this->assertEquals(5.25,$this->Calculation->getABV(1.040,1.000));
    }

    /**
     * Test getFermentCompletionPercentage method
     *
     * @return void
     */
    public function testGetFermentCompletionPercentage()
    {
        $batch = $this->Batches->newEntity([
            'brew_date' => new Time('2 days ago'),
            'package_date' => new Time('2 days'),
        ]);
        $this->assertEquals(25.0,$this->Calculation->getFermentCompletionPercentage($batch));
    }

    public function testGetFermentCompletionPercentageNoDates()
    {
        $batch = $this->Batches->newEntity([
        ]);
        $this->assertEquals(0,$this->Calculation->getFermentCompletionPercentage($batch));
    }

    /**
     * Test getPintsPerGallons method
     *
     * @return void
     */
    public function testGetPintsPerGallons()
    {
        $this->assertEquals(40,$this->Calculation->getPintsPerGallons(5));
    }
}
