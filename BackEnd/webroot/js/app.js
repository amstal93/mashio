
//    sidebar dropdown menu auto scrolling

jQuery('#sidebar .sub-menu > a').click(function () {
  var o = ($(this).offset());
  diff = 250 - o.top;
  if(diff>0)
      $("#sidebar").scrollTo("-="+Math.abs(diff),500);
  else
      $("#sidebar").scrollTo("+="+Math.abs(diff),500);
});

jQuery('.toggleLink').click(function(e) {
  e.preventDefault();
  $(e.target).siblings('.toggleTarget').slideToggle();
});


//    sidebar toggle

$(function() {
  function responsiveView() {
      var wSize = $(window).width();
      if (wSize <= 768) {
          $('#container').addClass('sidebar-close');
          $('#sidebar > ul').hide();
      }

      if (wSize > 768) {
          $('#container').removeClass('sidebar-close');
          $('#sidebar > ul').show();
      }
  }
  $(window).on('load', responsiveView);
  $(window).on('resize', responsiveView);
});

$('.fa-bars').click(function () {
  if ($('#sidebar > ul').is(":visible") === true) {
      $('#main-content').css({
          'margin-left': '0px'
      });
      $('#sidebar').css({
          'margin-left': '-210px'
      });
      $('#sidebar > ul').hide();
      $("#container").addClass("sidebar-closed");
  } else {
      $('#main-content').css({
          'margin-left': '210px'
      });
      $('#sidebar > ul').show();
      $('#sidebar').css({
          'margin-left': '0'
      });
      $("#container").removeClass("sidebar-closed");
  }
});

/*---LEFT BAR ACCORDION----*/
$(function() {
    $('#nav-accordion').dcAccordion({
        eventType: 'click',
        autoClose: true,
        saveState: true,
        disableLink: true,
        speed: 'fast',
        showCount: false,
        autoExpand: true,
        cookie: 'dcjq-accordion-1',
        classExpand: 'dcjq-current-parent'
    });
    $('.date-picker').datepicker({
        format: 'yyyy-mm-dd'
    });
    $('.abv-calc #fg, .abv-calc #og').keyup(function() {
        $('#abv').val(Math.round(($('#og').val()-$('#fg').val())*131.25*100,2)/100+'%');
    });
    $('.abv-calc #fg').trigger('keyup');

    $('select').formSelect();
});