<?php
use Migrations\AbstractMigration;

class AddFermenterToBatchEntries extends AbstractMigration
{

    public function up()
    {

        $this->table('batch_entries')
            ->addColumn('fermenter_id', 'integer', [
                'after' => 'user_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('fermenters')
            ->addColumn('status', 'integer', [
                'after' => 'user_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->addColumn('type', 'integer', [
                'after' => 'status',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('recipe_entries')
            ->addColumn('target_temperature', 'float', [
                'after' => 'user_id',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('fermenter_id', 'integer', [
                'after' => 'target_temperature',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('batch_entries')
            ->removeColumn('fermenter_id')
            ->update();

        $this->table('fermenters')
            ->removeColumn('status')
            ->removeColumn('type')
            ->update();

        $this->table('recipe_entries')
            ->removeColumn('target_temperature')
            ->removeColumn('fermenter_id')
            ->update();
    }
}

