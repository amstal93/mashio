<?php
use Migrations\AbstractMigration;

class AddingTaps extends AbstractMigration
{

    public function up()
    {

        $this->table('taps')
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('deleted', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('batch_id', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->create();

        $this->table('batches')
            ->addColumn('tapped_date', 'datetime', [
                'after' => 'user_id',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('kicked_date', 'datetime', [
                'after' => 'tapped_date',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('batches')
            ->removeColumn('tapped_date')
            ->removeColumn('kicked_date')
            ->update();

        $this->table('taps')->drop()->save();
    }
}

