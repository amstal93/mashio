<?php
use Migrations\AbstractMigration;

class AddingCategories extends AbstractMigration
{

    public function up()
    {

        $this->table('styles')
            ->changeColumn('og_min', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('og_max', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('fg_min', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('fg_max', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('abv_min', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->changeColumn('abv_max', 'float', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->update();

        $this->table('categories')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => true,
            ])
            ->addColumn('times_brewed', 'integer', [
                'default' => '0',
                'limit' => 11,
                'null' => true,
            ])
            ->create();

        $this->table('recipes')
            ->addColumn('times_brewed', 'integer', [
                'after' => 'description',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();

        $this->table('styles')
            ->addColumn('category_id', 'integer', [
                'after' => 'ingredients',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->addColumn('times_brewed', 'integer', [
                'after' => 'category_id',
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('recipes')
            ->removeColumn('times_brewed')
            ->update();

        $this->table('styles')
            ->changeColumn('og_min', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('og_max', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('fg_min', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('fg_max', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('abv_min', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->changeColumn('abv_max', 'integer', [
                'default' => null,
                'length' => 11,
                'null' => true,
            ])
            ->removeColumn('category_id')
            ->removeColumn('times_brewed')
            ->update();

        $this->table('categories')->drop()->save();
    }
}

