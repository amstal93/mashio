<?php
use Migrations\AbstractMigration;

class AddVolumeToRecipes extends AbstractMigration
{

    public function up()
    {

        $this->table('users')
            ->removeColumn('gallons_brewed')
            ->update();

        $this->table('recipes')
            ->addColumn('volume', 'float', [
                'after' => 'user_id',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->update();

        $this->table('users')
            ->addColumn('volume_brewed', 'float', [
                'after' => 'times_brewed',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->addColumn('display_metric', 'integer', [
                'after' => 'volume_brewed',
                'default' => null,
                'length' => 4,
                'null' => true,
            ])
            ->update();
    }

    public function down()
    {

        $this->table('recipes')
            ->removeColumn('volume')
            ->update();

        $this->table('users')
            ->addColumn('gallons_brewed', 'float', [
                'after' => 'times_brewed',
                'default' => null,
                'length' => null,
                'null' => true,
            ])
            ->removeColumn('volume_brewed')
            ->removeColumn('display_metric')
            ->update();
    }
}

